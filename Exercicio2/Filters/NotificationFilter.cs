﻿using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;

namespace Exercicio2.Apresentacao.Filters
{
    public class NotificationFilter : IAsyncResultFilter
    {
        private readonly INotificationBuilder _notificationBuilder;

        public NotificationFilter(INotificationBuilder notificationBuilder)
        {
            _notificationBuilder = notificationBuilder;
        }

        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if (_notificationBuilder.HasNotifications)
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.HttpContext.Response.ContentType = "application/json";

                var notifications = JsonConvert.SerializeObject(_notificationBuilder.Notifications);
                await context.HttpContext.Response.WriteAsync(notifications);

                return;
            }

            await next();
        }
    }
}
