﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Exercicio2.Apresentacao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SegurancaController : ControllerBase
    {
        private readonly IAplicacaoServicoSeguranca _aplicacaoServicoSeguranca;

        public SegurancaController(IAplicacaoServicoSeguranca AplicacaoServicoSeguranca)
        {
            _aplicacaoServicoSeguranca = AplicacaoServicoSeguranca;
        }

        /// <summary>
        /// Loga UsuarioDTO conforme enviado
        /// </summary>
        /// <param name="usuario">Objeto UsuarioDTO a ser logado</param>
        /// <returns>Objeto ConfiguracaoTokenRespostaDTO.</returns>
        [AllowAnonymous]
        [HttpPost]
        public object Post([FromBody]UsuarioDTO usuario)
        {
            if (!string.IsNullOrEmpty(usuario.Email) && !string.IsNullOrEmpty(usuario.Senha))
                return _aplicacaoServicoSeguranca.Login(usuario.Email, usuario.Senha);
            else
                return new { Mensagem = "Campo 'Email' e 'Senha' são obrigatórios." };
        }
    }
}