﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Exercicio2.Apresentacao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class MarcaController : ControllerBase
    {
        private readonly IAplicacaoServicoMarca _aplicacaoServicoMarca;
        public MarcaController(IAplicacaoServicoMarca AplicacaoServicoMarca)
        {
            _aplicacaoServicoMarca = AplicacaoServicoMarca;
        }

        /// <summary>
        /// Retorna objetos Marca
        /// </summary>
        /// <returns>Lista de objetos MarcaDTO</returns>
        // GET: api/Marca
        [Authorize("Bearer")]
        [HttpGet]
        public IEnumerable<MarcaDTO> Get()
        {
            return _aplicacaoServicoMarca.GetAll();
        }

        /// <summary>
        /// Retorna objeto Marca conforme Id solicitado
        /// </summary>
        /// <param name="id">Id da Marca</param>
        /// <returns>Objeto MarcaDTO.</returns>
        // GET: api/Marca/5
        [Authorize("Bearer")]
        [HttpGet("{id}")]
        public MarcaDTO Get(int id)
        {
            return _aplicacaoServicoMarca.GetById(id);
        }

        /// <summary>
        /// Cadastra objeto Marca conforme enviado
        /// </summary>
        /// <param name="marca">Objeto MarcaDTO a ser cadastrado</param>
        // POST: api/Marca
        [Authorize("Bearer")]
        [HttpPost]
        public void Post([FromBody]MarcaDTO marca)
        {
            _aplicacaoServicoMarca.Add(marca);
        }
        
        /// <summary>
        /// Atualiza objeto Marca conforme enviado
        /// </summary>
        /// <param name="marca">Objeto MarcaDTO a ser atualizado</param>
        // PUT: api/Marca
        [Authorize("Bearer")]
        [HttpPut]
        public void Put([FromBody]MarcaDTO marca)
        {
            _aplicacaoServicoMarca.Update(marca);
        }

        /// <summary>
        /// Deleta objeto Marca conforme enviado
        /// </summary>
        /// <param name="marca">Objeto MarcaDTO a ser deletado</param>
        // DELETE: api/Marca
        [Authorize("Bearer")]
        [HttpDelete]
        public void Delete([FromBody]MarcaDTO marca)
        {
            _aplicacaoServicoMarca.Remove(marca);
        }
    }
}