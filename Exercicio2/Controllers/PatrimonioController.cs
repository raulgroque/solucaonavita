﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Exercicio2.Apresentacao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class PatrimonioController : ControllerBase
    {
        private readonly IAplicacaoServicoPatrimonio _aplicacaoServicoPatrimonio;
        public PatrimonioController(IAplicacaoServicoPatrimonio AplicacaoServicoPatrimonio)
        {
            _aplicacaoServicoPatrimonio = AplicacaoServicoPatrimonio;
        }

        /// <summary>
        /// Retorna objetos Patrimonio
        /// </summary>
        /// <returns>Lista de objetos PatrimonioDTO</returns>
        // GET: api/Patrimonio
        [Authorize("Bearer")]
        [HttpGet]
        public IEnumerable<PatrimonioDTO> Get()
        {
            return _aplicacaoServicoPatrimonio.GetAll();
        }

        /// <summary>
        /// Retorna objeto Patrimonio conforme Id solicitado
        /// </summary>
        /// <param name="id">Id da Patrimonio</param>
        /// <returns>Objeto PatrimonioDTO.</returns>
        // GET: api/Patrimonio/5
        [Authorize("Bearer")]
        [HttpGet("{id}")]
        public PatrimonioDTO Get(int id)
        {
            return _aplicacaoServicoPatrimonio.GetById(id);
        }

        /// <summary>
        /// Cadastra objeto Patrimonio conforme enviado
        /// </summary>
        /// <param name="patrimonio">Objeto PatrimonioDTO a ser cadastrado</param>
        // POST: api/Patrimonio
        [Authorize("Bearer")]
        [HttpPost]
        public void Post([FromBody]PatrimonioDTO patrimonio)
        {
            _aplicacaoServicoPatrimonio.Add(patrimonio);
        }

        /// <summary>
        /// Atualiza objeto Patrimonio conforme enviado
        /// </summary>
        /// <param name="patrimonio">Objeto PatrimonioDTO a ser atualizado</param>
        // PUT: api/Patrimonio
        [Authorize("Bearer")]
        [HttpPut]
        public void Put([FromBody]PatrimonioDTO patrimonio)
        {
            _aplicacaoServicoPatrimonio.Update(patrimonio);
        }

        /// <summary>
        /// Deleta objeto Patrimonio conforme enviado
        /// </summary>
        /// <param name="patrimonio">Objeto PatrimonioDTO a ser deletado</param>
        // DELETE: api/Patrimonio
        [Authorize("Bearer")]
        [HttpDelete]
        public void Delete([FromBody]PatrimonioDTO patrimonio)
        {
            _aplicacaoServicoPatrimonio.Remove(patrimonio);
        }
    }
}
