﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exercicio2.Apresentacao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class LivroController : ControllerBase
    {
        private readonly IAplicacaoServicoLivro _aplicacaoServicoLivro;
        public LivroController(IAplicacaoServicoLivro AplicacaoServicoLivro)
        {
            _aplicacaoServicoLivro = AplicacaoServicoLivro;
        }

        /// <summary>
        /// Retorna objetos Livro
        /// </summary>
        /// <returns>Lista de objetos LivroDTO</returns>
        // GET: api/Livro
        [HttpGet]
        public IEnumerable<LivroDTO> Get()
        {
            return _aplicacaoServicoLivro.GetAll();
        }

        // GET: api/Livro/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        /// <summary>
        /// Cadastra objeto Usuario conforme enviado
        /// </summary>
        /// <param name="livro">Objeto UsuarioDTO a ser cadastrado</param>
        // POST: api/Livro
        [AllowAnonymous]
        [HttpPost]
        public void Post([FromBody]LivroDTO livro)
        {
            _aplicacaoServicoLivro.Add(livro);
        }

        // PUT: api/Livro/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
