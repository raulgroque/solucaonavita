﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Exercicio2.Apresentacao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class UsuarioController : ControllerBase
    {
        private readonly IAplicacaoServicoUsuario _aplicacaoServicoUsuario;
        public UsuarioController(IAplicacaoServicoUsuario AplicacaoServicoUsuario)
        {
            _aplicacaoServicoUsuario = AplicacaoServicoUsuario;
        }

        /// <summary>
        /// Retorna objetos Usuario
        /// </summary>
        /// <returns>Lista de objetos UsuarioDTO</returns>
        // GET: api/Usuario
        [Authorize("Bearer")]
        [HttpGet]
        public IEnumerable<UsuarioDTO> Get()
        {
            return _aplicacaoServicoUsuario.GetAll();
        }

        /// <summary>
        /// Retorna objeto Usuario conforme Id solicitado
        /// </summary>
        /// <param name="id">Id da Usuario</param>
        /// <returns>Objeto UsuarioDTO.</returns>
        // GET: api/Usuario/5
        [Authorize("Bearer")]
        [HttpGet("{id}")]
        public UsuarioDTO Get(int id)
        {
            return _aplicacaoServicoUsuario.GetById(id);
        }

        /// <summary>
        /// Cadastra objeto Usuario conforme enviado
        /// </summary>
        /// <param name="usuario">Objeto UsuarioDTO a ser cadastrado</param>
        // POST: api/Usuario
        [AllowAnonymous]
        [HttpPost]
        public void Post([FromBody]UsuarioDTO usuario)
        {
            _aplicacaoServicoUsuario.Add(usuario);
        }

        /// <summary>
        /// Atualiza objeto Usuario conforme enviado
        /// </summary>
        /// <param name="usuario">Objeto UsuarioDTO a ser atualizado</param>
        // PUT: api/Usuario
        [Authorize("Bearer")]
        [HttpPut]
        public void Put([FromBody]UsuarioDTO usuario)
        {
            _aplicacaoServicoUsuario.Update(usuario);
        }

        /// <summary>
        /// Deleta objeto Usuario conforme enviado
        /// </summary>
        /// <param name="usuario">Objeto UsuarioDTO a ser deletado</param>
        // DELETE: api/Usuario
        [Authorize("Bearer")]
        [HttpDelete]
        public void Delete([FromBody]UsuarioDTO usuario)
        {
            _aplicacaoServicoUsuario.Remove(usuario);
        }
    }
}
