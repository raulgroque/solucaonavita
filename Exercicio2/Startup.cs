﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Exercicio2.Aplicacao.DTO.DTO.Token;
using Exercicio2.Apresentacao.Filters;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Interfaces;
using Exercicio2.Infraestrutura.CrossCutting.IOC;
using Exercicio2.Infraestrutura.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace Exercicio2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration["SqlConnection:SqlConnectionString"];
            services.AddDbContext<SqlContext>(options => options.UseLazyLoadingProxies().UseSqlServer(connection));
            services.AddScoped<MongoContext>();

            services.AddOptions();

            services.AddMemoryCache();
            services.AddMvc(options => options.Filters.Add<NotificationFilter>())
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            ConfigureSwagger(services);
            ConfigureTokenJWT(services);

            return new AutofacServiceProvider(ConfigureAutoFac(services).Build());
        }

        private ContainerBuilder ConfigureAutoFac(IServiceCollection services)
        {
            var container = new ContainerBuilder();
            container.Populate(services);
            container.RegisterModule(new ModuloIOC());

            return container;
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            try
            {
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1",
                        new OpenApiInfo
                        {
                            Title = "Solução Navita",
                            Version = "v1",
                            Description = "Exemplo de API REST criada com o ASP.NET Core",
                            Contact = new OpenApiContact
                            {
                                Name = "Raul Roque",
                                Url = new Uri("https://gitlab.com/raulgroque")
                            }
                        });

                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                    c.IncludeXmlComments(xmlPath);
                });
            }
            catch(Exception) { }
        }

        private void ConfigureTokenJWT(IServiceCollection services)
        {
            var signingConfigurations = new ConfiguracaoSigningDTO();
            services.AddSingleton(signingConfigurations);

            var configuracaoToken = new ConfiguracaoTokenDTO();
            new ConfigureFromConfigurationOptions<ConfiguracaoTokenDTO>(
                Configuration.GetSection("TokenConfigurations"))
                    .Configure(configuracaoToken);
            services.AddSingleton(configuracaoToken);

            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(bearerOptions =>
            {
                var paramsValidation = bearerOptions.TokenValidationParameters;
                paramsValidation.IssuerSigningKey = signingConfigurations.Key;
                paramsValidation.ValidAudience = configuracaoToken.Audience;
                paramsValidation.ValidIssuer = configuracaoToken.Issuer;
                paramsValidation.ValidateIssuerSigningKey = true;
                paramsValidation.ValidateLifetime = true;
                paramsValidation.ClockSkew = TimeSpan.Zero;
            });

            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
                    .RequireAuthenticatedUser().Build());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json",
                    "Solucao Navita");
            });
        }
    }
}
