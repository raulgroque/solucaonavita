﻿using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Models;
using System;
using System.Linq.Expressions;

namespace Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications
{
    public class ExpressionSpecification<T> : SpecificationBuilder<T>
    {
        public override Notification Notification { get; set; }

        private readonly Expression<Func<T, bool>> _predicate;

        public override Expression<Func<T, bool>> Predicate { get => _predicate; }

        public ExpressionSpecification(Expression<Func<T, bool>> predicate)
        {
            _predicate = predicate;
        }
    }
}
