﻿using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Models;
using System;
using System.Linq.Expressions;

namespace Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications
{
    public class NullSpecification<T> : SpecificationBuilder<T>
    {
        public override Notification Notification { get; set; }
        public override Expression<Func<T, bool>> Predicate { get; }

        public NullSpecification()
        {
        }
    }
}
