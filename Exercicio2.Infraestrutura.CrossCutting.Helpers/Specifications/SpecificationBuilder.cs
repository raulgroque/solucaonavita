﻿using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Models;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications
{
    public abstract class SpecificationBuilder<T> : ISpecification<T>
    {
        public abstract Notification Notification { get; set; }
        public abstract Expression<Func<T, bool>> Predicate { get; }

        protected SpecificationBuilder()
        {
        }

        public static SpecificationBuilder<T> Create()
        {
            return new NullSpecification<T>();
        }

        public bool IsSatisfiedBy(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (Predicate == null)
            {
                throw new InvalidOperationException("Predicate cannot be null");
            }

            var predicate = Predicate.Compile();
            return predicate(entity);
        }

        public T SatisfyingItemFrom(IQueryable<T> query)
        {
            return Prepare(query).SingleOrDefault();
        }

        public IQueryable<T> SatisfyingItemsFrom(IQueryable<T> query)
        {
            return Prepare(query);
        }

        public IQueryable<T> Prepare(IQueryable<T> query)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            if (Predicate == null)
            {
                throw new InvalidOperationException("Predicate cannot be null");
            }

            var q = query.Where(Predicate);
            return q;
        }

        public ISpecification<T> Init(Expression<Func<T, bool>> expression)
        {
            return new ExpressionSpecification<T>(expression);
        }

        public ISpecification<T> InitEmpty()
        {
            return new NullSpecification<T>();
        }

        public ISpecification<T> And(ISpecification<T> other)
        {
            return new AndSpecification<T>(this, other);
        }

        public ISpecification<T> And(Expression<Func<T, bool>> other)
        {
            if (other == null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            return new AndSpecification<T>(this, new ExpressionSpecification<T>(other));
        }

        public ISpecification<T> Or(ISpecification<T> other)
        {
            return new OrSpecification<T>(this, other);
        }

        public ISpecification<T> Or(Expression<Func<T, bool>> other)
        {
            if (other == null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            return new OrSpecification<T>(this, new ExpressionSpecification<T>(other));
        }

        public ISpecification<T> Not()
        {
            return new NotSpecification<T>(this);
        }

        protected class SwapVisitor : ExpressionVisitor
        {
            private readonly Expression from, to;

            public SwapVisitor(Expression from, Expression to)
            {
                this.from = from;
                this.to = to;
            }

            public override Expression Visit(Expression node)
            {
                return node == from ? to : base.Visit(node);
            }
        }
    }
}
