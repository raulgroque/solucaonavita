﻿namespace Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces
{
    public interface ISpecificationFactory
    {
        ISpecification<T> Create<T>();
    }
}
