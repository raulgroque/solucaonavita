﻿using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Models;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications
{
    public class NotSpecification<T> : SpecificationBuilder<T>
    {
        private readonly ISpecification<T> _left;

        public override Notification Notification { get; set; }

        public override Expression<Func<T, bool>> Predicate
        {
            get
            {
                return Not(_left.Predicate);
            }
        }

        public NotSpecification(ISpecification<T> left)
        {
            _left = left ?? throw new ArgumentNullException(nameof(left));
        }

        private static Expression<Func<T, bool>> Not(Expression<Func<T, bool>> left)
        {
            if (left == null)
            {
                throw new ArgumentNullException(nameof(left));
            }

            var notExpression = Expression.Not(left.Body);
            var lambda = Expression.Lambda<Func<T, bool>>(notExpression, left.Parameters.Single());
            return lambda;
        }
    }
}
