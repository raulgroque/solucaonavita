﻿using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;

namespace Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications
{
    public class SpecificationFactory : ISpecificationFactory
    {
        public ISpecification<T> Create<T>()
        {
            return new NullSpecification<T>();
        }
    }
}
