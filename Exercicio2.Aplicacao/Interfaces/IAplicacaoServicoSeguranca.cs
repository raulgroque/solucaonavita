﻿using Exercicio2.Aplicacao.DTO.DTO.Token;

namespace Exercicio2.Aplicacao.Interfaces
{
    public interface IAplicacaoServicoSeguranca
    {
        ConfiguracaoTokenRespostaDTO Login(string login, string senha);
    }
}
