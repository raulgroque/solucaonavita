﻿using Exercicio2.Aplicacao.DTO.DTO;
using System.Collections.Generic;

namespace Exercicio2.Aplicacao.Interfaces
{
    public interface IAplicacaoServicoMarca
    {
        void Add(MarcaDTO obj);

        MarcaDTO GetById(int id);

        IEnumerable<MarcaDTO> GetAll();

        void Update(MarcaDTO obj);

        void Remove(MarcaDTO obj);

        void Dispose();
    }
}
