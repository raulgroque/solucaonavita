﻿using Exercicio2.Aplicacao.DTO.DTO;
using System.Collections.Generic;

namespace Exercicio2.Aplicacao.Interfaces
{
    public interface IAplicacaoServicoUsuario
    {
        void Add(UsuarioDTO obj);

        UsuarioDTO GetById(int id);

        IEnumerable<UsuarioDTO> GetAll();

        void Update(UsuarioDTO obj);

        void Remove(UsuarioDTO obj);

        void Dispose();

        UsuarioDTO GetByLoginPassword(string login, string password);
    }
}
