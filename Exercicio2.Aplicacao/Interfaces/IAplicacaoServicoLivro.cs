﻿using Exercicio2.Aplicacao.DTO.DTO;
using System.Collections.Generic;

namespace Exercicio2.Aplicacao.Interfaces
{
    public interface IAplicacaoServicoLivro
    {
        void Add(LivroDTO obj);

        LivroDTO GetById(int id);

        IEnumerable<LivroDTO> GetAll();

        void Update(LivroDTO obj);

        void Remove(LivroDTO obj);

        void Dispose();
    }
}
