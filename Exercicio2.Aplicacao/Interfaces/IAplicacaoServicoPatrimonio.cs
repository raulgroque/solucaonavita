﻿using Exercicio2.Aplicacao.DTO.DTO;
using System.Collections.Generic;

namespace Exercicio2.Aplicacao.Interfaces
{
    public interface IAplicacaoServicoPatrimonio
    {
        void Add(PatrimonioDTO obj);

        PatrimonioDTO GetById(int id);

        IEnumerable<PatrimonioDTO> GetAll();

        void Update(PatrimonioDTO obj);

        void Remove(PatrimonioDTO obj);

        void Dispose();
    }
}
