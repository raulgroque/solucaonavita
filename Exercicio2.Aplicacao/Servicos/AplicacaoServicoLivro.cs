﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.Interfaces;
using Exercicio2.Dominio.Core.Interfaces.Servicos;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces;
using System.Collections.Generic;

namespace Exercicio2.Aplicacao.Servicos
{
    public class AplicacaoServicoLivro : IAplicacaoServicoLivro
    {
        private readonly IServicoLivro _serviceLivro;
        private readonly IMapperLivro _mapperLivro;

        public AplicacaoServicoLivro(IServicoLivro ServicoLivro, IMapperLivro MapperLivro)
        {
            _serviceLivro = ServicoLivro;
            _mapperLivro = MapperLivro;
        }

        public void Add(LivroDTO obj)
        {
            var objLivro = _mapperLivro.MapperToEntity(obj);
            _serviceLivro.Add(objLivro);
        }

        public void Dispose()
        {
            _serviceLivro.Dispose();
        }

        public IEnumerable<LivroDTO> GetAll()
        {
            var objProdutos = _serviceLivro.GetAll();
            return _mapperLivro.MapperListLivros(objProdutos);
        }

        public LivroDTO GetById(int id)
        {
            var objLivro = _serviceLivro.GetById(id);
            return _mapperLivro.MapperToDTO(objLivro);
        }

        public void Remove(LivroDTO obj)
        {
            var objLivro = _mapperLivro.MapperToEntity(obj);
            _serviceLivro.Remove(objLivro);
        }

        public void Update(LivroDTO obj)
        {
            var objLivro = _mapperLivro.MapperToEntity(obj);
            _serviceLivro.Update(objLivro);
        }
    }
}
