﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.Interfaces;
using Exercicio2.Dominio.Core.Interfaces.Servicos;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces;
using System.Collections.Generic;

namespace Exercicio2.Aplicacao.Servicos
{
    public class AplicacaoServicoMarca : IAplicacaoServicoMarca
    {
        private readonly IServicoMarca _serviceMarca;
        private readonly IMapperMarca _mapperMarca;

        public AplicacaoServicoMarca(IServicoMarca ServicoMarca, IMapperMarca MapperMarca)
        {
            _serviceMarca = ServicoMarca;
            _mapperMarca = MapperMarca;
        }

        public void Add(MarcaDTO obj)
        {
            var objMarca = _mapperMarca.MapperToEntity(obj);
            _serviceMarca.Add(objMarca);
        }

        public void Dispose()
        {
            _serviceMarca.Dispose();
        }

        public IEnumerable<MarcaDTO> GetAll()
        {
            var objProdutos = _serviceMarca.GetAll();
            return _mapperMarca.MapperListMarcas(objProdutos);
        }

        public MarcaDTO GetById(int id)
        {
            var objMarca = _serviceMarca.GetById(id);
            return _mapperMarca.MapperToDTO(objMarca);
        }

        public void Remove(MarcaDTO obj)
        {
            var objMarca = _mapperMarca.MapperToEntity(obj);
            _serviceMarca.Remove(objMarca);
        }

        public void Update(MarcaDTO obj)
        {
            var objMarca = _mapperMarca.MapperToEntity(obj);
            _serviceMarca.Update(objMarca);
        }
    }
}
