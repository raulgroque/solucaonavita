﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.DTO.DTO.Token;
using Exercicio2.Aplicacao.Interfaces;
using Exercicio2.Infraestrutura.CrossCutting.Helpers;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;

namespace Exercicio2.Aplicacao.Servicos
{
    public class AplicacaoServicoSeguranca : IAplicacaoServicoSeguranca
    {
        private readonly IAplicacaoServicoUsuario _aplicacaoServicoUsuario;
        private readonly ConfiguracaoSigningDTO _configuracaoSigning;
        private readonly ConfiguracaoTokenDTO _configuracaoToken;

        public AplicacaoServicoSeguranca(
            ConfiguracaoSigningDTO configuracaoSigning,
            ConfiguracaoTokenDTO configuracaoToken,
            IAplicacaoServicoUsuario aplicacaoServicoUsuario)
        {
            _configuracaoSigning = configuracaoSigning;
            _configuracaoToken = configuracaoToken;
            _aplicacaoServicoUsuario = aplicacaoServicoUsuario;
        }

        public ConfiguracaoTokenRespostaDTO Login(string login, string senha)
        {
            var passwordHash = Util.GetMd5Hash(senha);
            UsuarioDTO usuarioDTO = _aplicacaoServicoUsuario.GetByLoginPassword(login, passwordHash);

            if (usuarioDTO != null)
            {
                ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(login, "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, login)
                    }
                );

                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao +
                    TimeSpan.FromSeconds(_configuracaoToken.Seconds);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = _configuracaoToken.Issuer,
                    Audience = _configuracaoToken.Audience,
                    SigningCredentials = _configuracaoSigning.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });
                var token = handler.WriteToken(securityToken);

                return new ConfiguracaoTokenRespostaDTO
                {
                    Autenticado = true,
                    DataCriacao = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                    DataExpiracao = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                    TokenAcesso = token,
                    Mensagem = "OK"
                };
            }
            else
            {
                return new ConfiguracaoTokenRespostaDTO
                {
                    Autenticado = false,
                    Mensagem = "Erro ao autenticar"
                };
            }
        }
    }
}
