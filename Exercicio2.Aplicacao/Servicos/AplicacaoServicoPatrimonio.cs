﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.Interfaces;
using Exercicio2.Dominio.Core.Interfaces.Servicos;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces;
using System.Collections.Generic;

namespace Exercicio2.Aplicacao.Servicos
{
    public class AplicacaoServicoPatrimonio : IAplicacaoServicoPatrimonio
    {
        private readonly IServicoPatrimonio _servicePatrimonio;
        private readonly IMapperPatrimonio _mapperPatrimonio;

        public AplicacaoServicoPatrimonio(IServicoPatrimonio ServicoPatrimonio, 
            IMapperPatrimonio MapperPatrimonio)
        {
            _servicePatrimonio = ServicoPatrimonio;
            _mapperPatrimonio = MapperPatrimonio;
        }

        public void Add(PatrimonioDTO obj)
        {
            var objPatrimonio = _mapperPatrimonio.MapperToEntity(obj);
            _servicePatrimonio.Add(objPatrimonio);
        }

        public void Dispose()
        {
            _servicePatrimonio.Dispose();
        }

        public IEnumerable<PatrimonioDTO> GetAll()
        {
            var objProdutos = _servicePatrimonio.GetAll();
            return _mapperPatrimonio.MapperListPatrimonios(objProdutos);
        }

        public PatrimonioDTO GetById(int id)
        {
            var objPatrimonio = _servicePatrimonio.GetById(id);
            return _mapperPatrimonio.MapperToDTO(objPatrimonio);
        }

        public void Remove(PatrimonioDTO obj)
        {
            var objPatrimonio = _mapperPatrimonio.MapperToEntity(obj);
            _servicePatrimonio.Remove(objPatrimonio);
        }

        public void Update(PatrimonioDTO obj)
        {
            var objPatrimonio = _mapperPatrimonio.MapperToEntity(obj);
            _servicePatrimonio.Update(objPatrimonio);
        }
    }
}
