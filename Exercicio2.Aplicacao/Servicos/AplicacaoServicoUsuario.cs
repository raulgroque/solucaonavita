﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Aplicacao.Interfaces;
using Exercicio2.Dominio.Core.Interfaces.Servicos;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Interfaces;
using System.Collections.Generic;

namespace Exercicio2.Aplicacao.Servicos
{
    public class AplicacaoServicoUsuario : IAplicacaoServicoUsuario
    {
        private readonly INotificationBuilder _notificationBuilder;
        private readonly IServicoUsuario _serviceUsuario;
        private readonly IMapperUsuario _mapperUsuario;

        public AplicacaoServicoUsuario(
            IServicoUsuario ServicoUsuario,
            IMapperUsuario MapperUsuario,
            INotificationBuilder NotificationBuilder
            )
        {
            _serviceUsuario = ServicoUsuario;
            _mapperUsuario = MapperUsuario;
            _notificationBuilder = NotificationBuilder;
        }

        public void Add(UsuarioDTO obj)
        {
            var objUsuario = _mapperUsuario.MapperToEntity(obj);

            if (objUsuario.Valid)
                _serviceUsuario.Add(objUsuario);
            else
                _notificationBuilder.AddNotifications(objUsuario.ValidationResult);
        }

        public void Dispose()
        {
            _serviceUsuario.Dispose();
        }

        public IEnumerable<UsuarioDTO> GetAll()
        {
            var objProdutos = _serviceUsuario.GetAll();
            return _mapperUsuario.MapperListUsuarios(objProdutos);
        }

        public UsuarioDTO GetById(int id)
        {
            var objUsuario = _serviceUsuario.GetById(id);
            return _mapperUsuario.MapperToDTO(objUsuario);
        }

        public UsuarioDTO GetByLoginPassword(string login, string password)
        {
            var objUsuario = _serviceUsuario.GetByLoginPassword(login, password);
            return _mapperUsuario.MapperToDTO(objUsuario);
        }

        public void Remove(UsuarioDTO obj)
        {
            var objUsuario = _mapperUsuario.MapperToEntity(obj);
            _serviceUsuario.Remove(objUsuario);
        }

        public void Update(UsuarioDTO obj)
        {
            var objUsuario = _mapperUsuario.MapperToEntity(obj);
            _serviceUsuario.Update(objUsuario);
        }
    }
}
