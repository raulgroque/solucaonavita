﻿using Exercicio2.Dominio.Modelos.Sql;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Models;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;

namespace Exercicio2.Dominio.Servicos.Especificacoes
{
    public static class UsuarioEspecificacaoExtensions
    {
        public static ISpecification<Usuario> TemEmail(this ISpecification<Usuario> specification, 
            string email)
        {
            var spec = specification.And(x => x.Email == email);
            spec.Notification = new Notification("UsuarioTemEmail", "Usuário já tem e-mail cadastrado!");

            return spec;
        }
    }
}
