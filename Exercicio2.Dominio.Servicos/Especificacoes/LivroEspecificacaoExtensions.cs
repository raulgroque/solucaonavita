﻿using Exercicio2.Dominio.Modelos.Mongo;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Models;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;

namespace Exercicio2.Dominio.Servicos.Especificacoes
{
    public static class LivroEspecificacaoExtensions
    {
        public static ISpecification<Livro> TemAutor(this ISpecification<Livro> specification, 
            string autor)
        {
            var spec = specification.And(x => x.Autor == autor);
            spec.Notification = new Notification("LivroTemAutor", "Livro já tem autor cadastrado!");

            return spec;
        }
    }
}
