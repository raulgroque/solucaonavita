﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Dominio.Core.Interfaces.Servicos;
using Exercicio2.Dominio.Modelos.Sql;

namespace Exercicio2.Dominio.Servicos.Servicos
{
    public class ServicoPatrimonio : ServicoBase<Patrimonio>, IServicoPatrimonio
    {
        public readonly IRepositorioPatrimonio _repositorioPatrimonio;

        public ServicoPatrimonio(IRepositorioPatrimonio RepositorioPatrimonio)
            : base(RepositorioPatrimonio)
        {
            _repositorioPatrimonio = RepositorioPatrimonio;
        }
    }
}
