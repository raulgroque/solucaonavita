﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Dominio.Core.Interfaces.Servicos;
using Exercicio2.Dominio.Modelos.Mongo;
using Exercicio2.Dominio.Servicos.Especificacoes;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Interfaces;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;

namespace Exercicio2.Dominio.Servicos.Servicos
{
    public class ServicoLivro : ServicoBase<Livro>, IServicoLivro
    {
        private readonly IRepositorioLivro _repositorioLivro;
        private readonly INotificationBuilder _notificationBuilder;
        private readonly ISpecificationFactory _specificationFactory;

        public ServicoLivro(IRepositorioLivro RepositorioLivro,
            INotificationBuilder NotificationBuilder,
            ISpecificationFactory SpecificationFactory)
            : base(RepositorioLivro)
        {
            _repositorioLivro = RepositorioLivro;
            _specificationFactory = SpecificationFactory;
            _notificationBuilder = NotificationBuilder;
        }

        public override void Add(Livro obj)
        {
            var especTemAutor = _specificationFactory.Create<Livro>().TemAutor(obj.Autor);
            var existeUsuario = _repositorioLivro.Find(especTemAutor);

            if (existeUsuario.Count == 0)
                base.Add(obj);
            else
                _notificationBuilder.AddNotification(especTemAutor.Notification);
        }
    }
}
