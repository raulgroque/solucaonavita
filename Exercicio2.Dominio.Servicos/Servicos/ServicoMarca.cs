﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Dominio.Core.Interfaces.Servicos;
using Exercicio2.Dominio.Modelos.Sql;

namespace Exercicio2.Dominio.Servicos.Servicos
{
    public class ServicoMarca : ServicoBase<Marca>, IServicoMarca
    {
        public readonly IRepositorioMarca _repositorioMarca;

        public ServicoMarca(IRepositorioMarca RepositorioMarca)
            : base(RepositorioMarca)
        {
            _repositorioMarca = RepositorioMarca;
        }
    }
}
