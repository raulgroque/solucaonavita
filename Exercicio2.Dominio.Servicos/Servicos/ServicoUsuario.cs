﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Dominio.Core.Interfaces.Servicos;
using Exercicio2.Dominio.Modelos.Sql;
using Exercicio2.Dominio.Servicos.Especificacoes;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Interfaces;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;

namespace Exercicio2.Dominio.Servicos.Servicos
{
    public class ServicoUsuario : ServicoBase<Usuario>, IServicoUsuario
    {
        private readonly IRepositorioUsuario _repositorioUsuario;
        private readonly INotificationBuilder _notificationBuilder;
        private readonly ISpecificationFactory _specificationFactory;

        public ServicoUsuario(IRepositorioUsuario RepositorioUsuario,
            INotificationBuilder NotificationBuilder,
            ISpecificationFactory SpecificationFactory)
            : base(RepositorioUsuario)
        {
            _repositorioUsuario = RepositorioUsuario;
            _notificationBuilder = NotificationBuilder;
            _specificationFactory = SpecificationFactory;
        }

        public override void Add(Usuario obj)
        {
            var especTemEmail = _specificationFactory.Create<Usuario>().TemEmail(obj.Email);
            var existeUsuario = _repositorioUsuario.Find(especTemEmail);

            if (existeUsuario.Count == 0)
                base.Add(obj);
            else
                _notificationBuilder.AddNotification(especTemEmail.Notification);
        }

        public Usuario GetByLoginPassword(string login, string password)
        {
            return _repositorioUsuario.GetByLoginPassword(login, password);
        }
    }
}
