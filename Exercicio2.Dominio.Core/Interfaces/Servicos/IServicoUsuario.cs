﻿using Exercicio2.Dominio.Modelos.Sql;

namespace Exercicio2.Dominio.Core.Interfaces.Servicos
{
    public interface IServicoUsuario : IServicoBase<Usuario>
    {
        Usuario GetByLoginPassword(string login, string password);
    }
}
