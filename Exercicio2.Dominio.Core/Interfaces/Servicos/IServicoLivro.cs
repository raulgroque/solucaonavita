﻿using Exercicio2.Dominio.Modelos.Mongo;

namespace Exercicio2.Dominio.Core.Interfaces.Servicos
{
    public interface IServicoLivro : IServicoBase<Livro>
    {
    }
}
