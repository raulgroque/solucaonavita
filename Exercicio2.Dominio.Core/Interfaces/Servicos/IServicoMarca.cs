﻿using Exercicio2.Dominio.Modelos.Sql;

namespace Exercicio2.Dominio.Core.Interfaces.Servicos
{
    public interface IServicoMarca : IServicoBase<Marca>
    {
    }
}
