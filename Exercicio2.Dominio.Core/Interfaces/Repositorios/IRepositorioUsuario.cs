﻿using Exercicio2.Dominio.Modelos.Sql;

namespace Exercicio2.Dominio.Core.Interfaces.Repositorios
{
    public interface IRepositorioUsuario : IRepositorioBase<Usuario>
    {
        Usuario GetByLoginPassword(string login, string password);
    }
}
