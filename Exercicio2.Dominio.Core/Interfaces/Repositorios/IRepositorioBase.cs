﻿using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio2.Dominio.Core.Interfaces.Repositorios
{
    public interface IRepositorioBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);

        TEntity GetById(int id);

        IEnumerable<TEntity> GetAll();

        void Update(TEntity obj);

        void Remove(TEntity obj);

        IReadOnlyList<TEntity> Find(ISpecification<TEntity> specification, int page = 0, int pageSize = 100);

        void Dispose();
    }
}
