﻿using Exercicio2.Dominio.Modelos.Mongo;

namespace Exercicio2.Dominio.Core.Interfaces.Repositorios
{
    public interface IRepositorioLivro : IRepositorioBase<Livro>
    {
    }
}
