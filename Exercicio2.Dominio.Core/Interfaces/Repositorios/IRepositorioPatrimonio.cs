﻿using Exercicio2.Dominio.Modelos.Sql;

namespace Exercicio2.Dominio.Core.Interfaces.Repositorios
{
    public interface IRepositorioPatrimonio : IRepositorioBase<Patrimonio>
    {
    }
}
