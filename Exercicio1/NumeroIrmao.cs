﻿using System;
using System.Linq;

namespace Exercicio1
{
    public static class NumeroIrmao
    {
        public static int CalcularNumeroIrmao(int pNumero)
        {
            try
            {
                if (pNumero < 0)
                    throw new InvalidOperationException("Não é permitido número negativo");

                var numeros = pNumero.ToString().ToCharArray();

                var maiorNumero = int.Parse(numeros.OrderByDescending(n => n)
                    .Select(n => n.ToString()).Aggregate((a, b) => a + b));

                return maiorNumero < 100000000 ? maiorNumero : -1;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
    }
}
