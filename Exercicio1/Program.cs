﻿using System;

namespace Exercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            var reiniciar = string.Empty;

            do
            {
                try
                {
                    Console.WriteLine("Escolha um número: ");

                    var keyInfo = Console.ReadLine();

                    if (int.TryParse(keyInfo, out int numero))
                        Console.WriteLine("Maior número irmão: " +
                            NumeroIrmao.CalcularNumeroIrmao(numero).ToString());
                    else
                        Console.WriteLine("Você não digitou um número válido");

                    Console.WriteLine();

                    Console.WriteLine("Reiniciar? Digite \"sim\". Caso contrário qualquer outra tecla.");
                    reiniciar = Console.ReadLine();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                if (reiniciar != "sim")
                    break;
            }
            while (reiniciar == "sim");
        }
    }
}
