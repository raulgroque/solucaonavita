#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.0-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.0-buster AS build
WORKDIR /src
COPY ["Exercicio2/Exercicio2.Apresentacao.csproj", "Exercicio2/"]
COPY ["Exercicio2.Infraestrutura.CrossCutting.IOC/Exercicio2.Infraestrutura.CrossCutting.IOC.csproj", "Exercicio2.Infraestrutura.CrossCutting.IOC/"]
COPY ["Exercicio2.Infraestrutura.Data.Repositorio/Exercicio2.Infraestrutura.Data.Repositorio.csproj", "Exercicio2.Infraestrutura.Data.Repositorio/"]
COPY ["Exercicio2.Dominio.Core/Exercicio2.Dominio.Core.csproj", "Exercicio2.Dominio.Core/"]
COPY ["Exercicio2.Dominio/Exercicio2.Dominio.csproj", "Exercicio2.Dominio/"]
COPY ["Exercicio2.Infraestrutura.Data/Exercicio2.Infraestrutura.Data.csproj", "Exercicio2.Infraestrutura.Data/"]
COPY ["Exercicio2.Aplicacao/Exercicio2.Aplicacao.csproj", "Exercicio2.Aplicacao/"]
COPY ["Exercicio2.Infraestrutura.CrossCutting.Helpers/Exercicio2.Infraestrutura.CrossCutting.Helpers.csproj", "Exercicio2.Infraestrutura.CrossCutting.Helpers/"]
COPY ["Exercicio2.Aplicacao.DTO/Exercicio2.Aplicacao.DTO.csproj", "Exercicio2.Aplicacao.DTO/"]
COPY ["Exercicio2.Infraestrutura.CrossCutting.Adaptador/Exercicio2.Infraestrutura.CrossCutting.Adaptador.csproj", "Exercicio2.Infraestrutura.CrossCutting.Adaptador/"]
COPY ["Exercicio2.Dominio.Servicos/Exercicio2.Dominio.Servicos.csproj", "Exercicio2.Dominio.Servicos/"]
RUN dotnet restore "Exercicio2/Exercicio2.Apresentacao.csproj"
COPY . .
WORKDIR "/src/Exercicio2"
RUN dotnet build "Exercicio2.Apresentacao.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Exercicio2.Apresentacao.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Exercicio2.Apresentacao.dll"]