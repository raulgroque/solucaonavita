﻿namespace Exercicio2.Aplicacao.DTO.DTO
{
    public class UsuarioDTO
    {
        public int UsuarioId { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Senha { get; set; }
    }
}
