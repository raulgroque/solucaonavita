﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio2.Aplicacao.DTO.DTO
{
    public class MarcaDTO
    {
        public int MarcaId { get; set; }

        public string Nome { get; set; }
    }
}
