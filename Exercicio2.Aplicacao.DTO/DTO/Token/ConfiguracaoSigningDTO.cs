﻿using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;

namespace Exercicio2.Aplicacao.DTO.DTO.Token
{
    public class ConfiguracaoSigningDTO
    {
        public SecurityKey Key { get; }
        public SigningCredentials SigningCredentials { get; }

        public ConfiguracaoSigningDTO()
        {
            using (var provider = new RSACryptoServiceProvider(2048))
            {
                Key = new RsaSecurityKey(provider.ExportParameters(true));
            }

            SigningCredentials = new SigningCredentials(
                Key, SecurityAlgorithms.RsaSha256Signature);
        }
    }
}
