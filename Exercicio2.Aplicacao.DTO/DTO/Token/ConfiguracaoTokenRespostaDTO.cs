﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio2.Aplicacao.DTO.DTO.Token
{
    public class ConfiguracaoTokenRespostaDTO
    {
        public bool Autenticado { get; set; }
        public string DataCriacao { get; set; }
        public string DataExpiracao { get; set; }
        public string TokenAcesso { get; set; }
        public string Mensagem { get; set; }
    }
}
