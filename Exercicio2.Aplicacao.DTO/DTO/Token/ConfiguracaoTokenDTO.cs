﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio2.Aplicacao.DTO.DTO.Token
{
    public class ConfiguracaoTokenDTO
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
    }
}
