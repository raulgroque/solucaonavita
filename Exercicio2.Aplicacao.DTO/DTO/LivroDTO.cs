﻿namespace Exercicio2.Aplicacao.DTO.DTO
{
    public class LivroDTO
    {
        public string LivroId { get; set; }

        public string Nome { get; set; }

        public decimal Preco { get; set; }

        public string Categoria { get; set; }

        public string Autor { get; set; }
    }
}
