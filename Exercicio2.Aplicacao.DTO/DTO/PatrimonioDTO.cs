﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio2.Aplicacao.DTO.DTO
{
    public class PatrimonioDTO
    {
        public int NumeroTombo { get; set; }

        public string Nome { get; set; }

        public int MarcaId { get; set; }

        public MarcaDTO Marca { get; set; }

        public string Descricao { get; set; }
    }
}
