﻿using FluentValidation;

namespace Exercicio2.Dominio.Modelos.Sql.Validadores
{
    public class UsuarioValidador : AbstractValidator<Usuario>
    {
        public UsuarioValidador()
        {
            RuleFor(a => a.Email)
                .NotEmpty()
                .EmailAddress()
                .WithMessage("E-mail inválido");

            RuleFor(a => a.Nome)
                .NotEmpty()
                .WithMessage("Nome não pode ser vazio");

            RuleFor(a => a.Senha)
                .NotEmpty()
                .WithMessage("Senha não pode ser vazia");
        }
    }
}
