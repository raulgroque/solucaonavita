﻿using Exercicio2.Dominio.Modelos.Sql.Validadores;
using Exercicio2.Infraestrutura.CrossCutting.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exercicio2.Dominio.Modelos.Sql
{
    public class Usuario : Entidade
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UsuarioId { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Senha { get; set; }

        public Usuario() { }

        public Usuario(int usuarioId, string nome, string email, string senha)
        {
            UsuarioId = usuarioId;
            Nome = nome;
            Email = email;
            Senha = !string.IsNullOrEmpty(senha) ? Util.GetMd5Hash(senha) : null;

            Validate(this, new UsuarioValidador());
        }
    }
}
