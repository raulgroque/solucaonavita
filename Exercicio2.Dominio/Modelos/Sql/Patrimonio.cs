﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exercicio2.Dominio.Modelos.Sql
{
    public class Patrimonio
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NumeroTombo { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required, ForeignKey("Marca")]
        public int MarcaId { get; set; }

        public virtual Marca Marca { get; set; }

        public string Descricao { get; set; }
    }
}
