﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Exercicio2.Dominio.Modelos.Mongo
{
    public class Livro
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string LivroId { get; set; }

        public string Nome { get; set; }

        public decimal Preco { get; set; }

        public string Categoria { get; set; }

        public string Autor { get; set; }
    }
}
