﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Dominio.Modelos.Sql;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces;
using System.Collections.Generic;

namespace Exercicio2.Infraestrutura.CrossCutting.Adaptador.Map
{
    public class MapperMarca : IMapperMarca
    {
        List<MarcaDTO> marcaDTOs = new List<MarcaDTO>();

        public Marca MapperToEntity(MarcaDTO MarcaDTO)
        {
            Marca marca = null;

            if (MarcaDTO != null)
            {
                marca = new Marca
                {
                    MarcaId = MarcaDTO.MarcaId,
                    Nome = MarcaDTO.Nome
                };
            }

            return marca;
        }

        public IEnumerable<MarcaDTO> MapperListMarcas(IEnumerable<Marca> marcas)
        {
            foreach (var item in marcas)
            {
                MarcaDTO marcaDTO = new MarcaDTO
                {
                    MarcaId = item.MarcaId,
                    Nome = item.Nome
                };

                marcaDTOs.Add(marcaDTO);
            }

            return marcaDTOs;
        }

        public MarcaDTO MapperToDTO(Marca Marca)
        {
            MarcaDTO marcaDTO = null;

            if (Marca != null)
            {
                marcaDTO = new MarcaDTO
                {
                    MarcaId = Marca.MarcaId,
                    Nome = Marca.Nome
                };
            }

            return marcaDTO;
        }
    }
}
