﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Dominio.Modelos.Sql;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces;
using System.Collections.Generic;

namespace Exercicio2.Infraestrutura.CrossCutting.Adaptador.Map
{
    public class MapperPatrimonio : IMapperPatrimonio
    {
        private readonly IMapperMarca _mapperMarca;

        public MapperPatrimonio(IMapperMarca mapperMarca)
        {
            _mapperMarca = mapperMarca;
        }

        List<PatrimonioDTO> patrimonioDTOs = new List<PatrimonioDTO>();

        public Patrimonio MapperToEntity(PatrimonioDTO PatrimonioDTO)
        {
            Patrimonio patrimonio = null;

            if (PatrimonioDTO != null)
            {
                patrimonio = new Patrimonio
                {
                    NumeroTombo = PatrimonioDTO.NumeroTombo,
                    Nome = PatrimonioDTO.Nome,
                    Descricao = PatrimonioDTO.Descricao,
                    MarcaId = PatrimonioDTO.MarcaId
                };
            }

            return patrimonio;
        }

        public IEnumerable<PatrimonioDTO> MapperListPatrimonios(IEnumerable<Patrimonio> patrimonios)
        {
            foreach (var item in patrimonios)
            {
                PatrimonioDTO patrimonioDTO = new PatrimonioDTO
                {
                    NumeroTombo = item.NumeroTombo,
                    Nome = item.Nome,
                    Descricao = item.Descricao,
                    MarcaId = item.MarcaId,
                    Marca = _mapperMarca.MapperToDTO(item.Marca)
                };

                patrimonioDTOs.Add(patrimonioDTO);
            }

            return patrimonioDTOs;
        }

        public PatrimonioDTO MapperToDTO(Patrimonio Patrimonio)
        {
            PatrimonioDTO patrimonioDTO = null;

            if (Patrimonio != null)
            {
                patrimonioDTO = new PatrimonioDTO
                {
                    NumeroTombo = Patrimonio.NumeroTombo,
                    Nome = Patrimonio.Nome,
                    Descricao = Patrimonio.Descricao,
                    MarcaId = Patrimonio.MarcaId,
                    Marca = _mapperMarca.MapperToDTO(Patrimonio.Marca)
                };
            }

            return patrimonioDTO;
        }
    }
}
