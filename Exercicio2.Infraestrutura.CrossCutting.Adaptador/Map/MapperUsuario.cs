﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Dominio.Modelos.Sql;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces;
using System.Collections.Generic;

namespace Exercicio2.Infraestrutura.CrossCutting.Adaptador.Map
{
    public class MapperUsuario : IMapperUsuario
    {
        List<UsuarioDTO> usuarioDTOs = new List<UsuarioDTO>();

        public Usuario MapperToEntity(UsuarioDTO UsuarioDTO)
        {
            Usuario usuario = null;

            if (UsuarioDTO != null)
            {
                usuario = new Usuario(
                    UsuarioDTO.UsuarioId,
                    UsuarioDTO.Nome,
                    UsuarioDTO.Email,
                    UsuarioDTO.Senha);
            }

            return usuario;
        }

        public IEnumerable<UsuarioDTO> MapperListUsuarios(IEnumerable<Usuario> usuarios)
        {
            foreach (var item in usuarios)
            {
                UsuarioDTO usuarioDTO = new UsuarioDTO
                {
                    UsuarioId = item.UsuarioId,
                    Nome = item.Nome,
                    Email = item.Email
                };

                usuarioDTOs.Add(usuarioDTO);
            }

            return usuarioDTOs;
        }

        public UsuarioDTO MapperToDTO(Usuario Usuario)
        {
            UsuarioDTO usuarioDTO = null;

            if (Usuario != null)
            {
                usuarioDTO = new UsuarioDTO
                {
                    UsuarioId = Usuario.UsuarioId,
                    Nome = Usuario.Nome,
                    Email = Usuario.Email
                };
            }

            return usuarioDTO;
        }
    }
}
