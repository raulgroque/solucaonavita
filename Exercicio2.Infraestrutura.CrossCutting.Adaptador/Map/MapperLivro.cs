﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Dominio.Modelos.Mongo;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces;
using System.Collections.Generic;

namespace Exercicio2.Infraestrutura.CrossCutting.Adaptador.Map
{
    public class MapperLivro : IMapperLivro
    {
        List<LivroDTO> livroDTOs = new List<LivroDTO>();

        public Livro MapperToEntity(LivroDTO LivroDTO)
        {
            Livro livro = null;

            if (LivroDTO != null)
            {
                livro = new Livro
                {
                    LivroId = LivroDTO.LivroId,
                    Nome = LivroDTO.Nome,
                    Preco = LivroDTO.Preco,
                    Categoria = LivroDTO.Categoria,
                    Autor = LivroDTO.Autor
                };
            }

            return livro;
        }

        public IEnumerable<LivroDTO> MapperListLivros(IEnumerable<Livro> livros)
        {
            foreach (var item in livros)
            {
                LivroDTO livroDTO = new LivroDTO
                {
                    LivroId = item.LivroId,
                    Nome = item.Nome,
                    Preco = item.Preco,
                    Categoria = item.Categoria,
                    Autor = item.Autor
                };

                livroDTOs.Add(livroDTO);
            }

            return livroDTOs;
        }

        public LivroDTO MapperToDTO(Livro Livro)
        {
            LivroDTO livroDTO = null;

            if (Livro != null)
            {
                livroDTO = new LivroDTO
                {
                    LivroId = Livro.LivroId,
                    Nome = Livro.Nome,
                    Preco = Livro.Preco,
                    Categoria = Livro.Categoria,
                    Autor = Livro.Autor
                };
            }

            return livroDTO;
        }
    }
}
