﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Dominio.Modelos.Mongo;
using System.Collections.Generic;

namespace Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces
{
    public interface IMapperLivro
    {
        Livro MapperToEntity(LivroDTO marcaDTO);

        IEnumerable<LivroDTO> MapperListLivros(IEnumerable<Livro> marcas);

        LivroDTO MapperToDTO(Livro marca);
    }
}
