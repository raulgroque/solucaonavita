﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Dominio.Modelos.Sql;
using System.Collections.Generic;

namespace Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces
{
    public interface IMapperUsuario
    {
        Usuario MapperToEntity(UsuarioDTO usuarioDTO);

        IEnumerable<UsuarioDTO> MapperListUsuarios(IEnumerable<Usuario> usuarios);

        UsuarioDTO MapperToDTO(Usuario usuario);
    }
}
