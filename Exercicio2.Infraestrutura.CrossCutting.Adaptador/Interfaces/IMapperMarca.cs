﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Dominio.Modelos.Sql;
using System.Collections.Generic;

namespace Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces
{
    public interface IMapperMarca
    {
        Marca MapperToEntity(MarcaDTO marcaDTO);

        IEnumerable<MarcaDTO> MapperListMarcas(IEnumerable<Marca> marcas);

        MarcaDTO MapperToDTO(Marca marca);
    }
}
