﻿using Exercicio2.Aplicacao.DTO.DTO;
using Exercicio2.Dominio.Modelos.Sql;
using System.Collections.Generic;

namespace Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces
{
    public interface IMapperPatrimonio
    {
        Patrimonio MapperToEntity(PatrimonioDTO patrimonioDTO);

        IEnumerable<PatrimonioDTO> MapperListPatrimonios(IEnumerable<Patrimonio> patrimonios);

        PatrimonioDTO MapperToDTO(Patrimonio patrimonio);
    }
}
