using Exercicio1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestesUnitarios
{
    [TestClass]
    public class TesteUnitario1
    {
        [TestMethod]
        public void TestarNumero213()
        {
            var numero = NumeroIrmao.CalcularNumeroIrmao(213);
            Assert.AreEqual(321, numero);
        }

        [TestMethod]
        public void TestarNumero553()
        {
            var numero = NumeroIrmao.CalcularNumeroIrmao(553);
            Assert.AreEqual(553, numero);
        }

        [TestMethod]
        public void TestarNumero100000000()
        {
            var numero = NumeroIrmao.CalcularNumeroIrmao(100000000);
            Assert.AreEqual(-1, numero);
        }

        [TestMethod]
        public void TestarNumero100000011()
        {
            var numero = NumeroIrmao.CalcularNumeroIrmao(100000011);
            Assert.AreEqual(-1, numero);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException),
            "N�o houve exce��o de n�o � permitido n�mero negativo!")]
        public void TestarNumeroNegativo()
        {
            NumeroIrmao.CalcularNumeroIrmao(-1);
        }
    }
}
