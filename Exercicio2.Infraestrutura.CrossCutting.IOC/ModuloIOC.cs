﻿using Autofac;

namespace Exercicio2.Infraestrutura.CrossCutting.IOC
{
    public class ModuloIOC : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            ConfiguracaoIOC.Load(builder);
        }
    }
}
