﻿using Autofac;
using Exercicio2.Aplicacao.Interfaces;
using Exercicio2.Aplicacao.Servicos;
using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Dominio.Core.Interfaces.Servicos;
using Exercicio2.Dominio.Servicos.Servicos;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Interfaces;
using Exercicio2.Infraestrutura.CrossCutting.Adaptador.Map;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Notifications.Interfaces;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;
using Exercicio2.Infraestrutura.Data.Repositorio;

namespace Exercicio2.Infraestrutura.CrossCutting.IOC
{
    public class ConfiguracaoIOC
    {
        public static void Load(ContainerBuilder builder)
        {
            #region IOC Aplicacoes
            builder.RegisterType<AplicacaoServicoPatrimonio>().As<IAplicacaoServicoPatrimonio>();
            builder.RegisterType<AplicacaoServicoMarca>().As<IAplicacaoServicoMarca>();
            builder.RegisterType<AplicacaoServicoUsuario>().As<IAplicacaoServicoUsuario>();
            builder.RegisterType<AplicacaoServicoSeguranca>().As<IAplicacaoServicoSeguranca>();

            builder.RegisterType<AplicacaoServicoLivro>().As<IAplicacaoServicoLivro>();
            #endregion

            #region IOC Servicos
            builder.RegisterType<ServicoPatrimonio>().As<IServicoPatrimonio>();
            builder.RegisterType<ServicoMarca>().As<IServicoMarca>();
            builder.RegisterType<ServicoUsuario>().As<IServicoUsuario>();

            builder.RegisterType<ServicoLivro>().As<IServicoLivro>();

            builder.RegisterType<NotificationBuilder>().As<INotificationBuilder>().InstancePerLifetimeScope();
            builder.RegisterType<SpecificationFactory>().As<ISpecificationFactory>();
            #endregion

            #region IOC Repositorios
            builder.RegisterType<RepositorioPatrimonio>().As<IRepositorioPatrimonio>();
            builder.RegisterType<RepositorioMarca>().As<IRepositorioMarca>();
            builder.RegisterType<RepositorioUsuario>().As<IRepositorioUsuario>();

            builder.RegisterType<RepositorioLivro>().As<IRepositorioLivro>();
            #endregion

            #region IOC Mappers
            builder.RegisterType<MapperPatrimonio>().As<IMapperPatrimonio>();
            builder.RegisterType<MapperMarca>().As<IMapperMarca>();
            builder.RegisterType<MapperUsuario>().As<IMapperUsuario>();

            builder.RegisterType<MapperLivro>().As<IMapperLivro>();
            #endregion
        }
    }
}
