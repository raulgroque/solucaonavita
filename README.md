- SolucaoNavita

-> Projeto Exercicio1
(Console Application .NET Core 3.0)
-> Projeto TestesUnitarios
(Unit Test .NET Core 3.0)

---

-> Projetos Exercicio2
(WebAPI .NET Core 3.0 + Class Libraries .NET Core 3.0)

-> IMPORTANTE: Favor alterar a propriedade SqlConnectionString no appsettings.json da WebAPI conforme a conexão desejada

-> Swagger: https://localhost:44375/swagger

-> EF Core Code First: Executar o comando "update-database" no Package Manager Console apontando para o projeto padrão "Exercicio2.Infraestrutura.Data"
para executar a migration de criação das tabelas.
-> OBS: Favor rodar o script "Script/Script_CreateDatabase_Navita.sql" apenas para criar a base antes de rodar o comando acima.

- Projeto separado em 4 camadas:
-> Apresentação
- Camada com 4 Controllers (Seguranca, Usuario, Patrimonio e Marca).
- Configuração do Entity Framework Core 3.1;
- Configuração do Swagger integrado a API;
- Configuração do AutoFac evitando exposição da IoC;

-> Aplicação
- Camada responsável por aplicar as regras de negócio de cada domínio;
- Contendo interfaces e implementações dos serviços de aplicação específicos referente a cada entidade;
- Contendo DTOs (Data transfer object) para transformação entre os modelos do domínio e da aplicação;

-> Dominio
- Camada responsável por organizar os modelos do projeto
- Contendo interfaces dos repositorios e serviços específicos referente a cada entidade;
- Contendo implementação dos serviços de domínio específicos referente a cada entidade;

-> Infraestrutura
- Camada dividida entre Data e CrossCutting
- Camada Data responsável por configurar o contexto SQL e as migrations do Entity Framework Core (Code First);
- Camada CrossCutting responsável pela gestão IoC das injeções de dependência usando AutoFac;
- Contendo também Mappers para transformações entre os modelos do domínio e os DTOs;
- Além de funções genéricas (Helpers) para aplicação nos demais projetos