﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Dominio.Modelos.Sql;

namespace Exercicio2.Infraestrutura.Data.Repositorio
{
    public class RepositorioMarca : RepositorioBaseSQL<Marca>, IRepositorioMarca
    {
        private readonly SqlContext _context;
        public RepositorioMarca(SqlContext Context)
            : base(Context)
        {
            _context = Context;
        }
    }
}
