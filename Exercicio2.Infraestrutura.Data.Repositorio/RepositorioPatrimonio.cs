﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Dominio.Modelos.Sql;

namespace Exercicio2.Infraestrutura.Data.Repositorio
{
    public class RepositorioPatrimonio : RepositorioBaseSQL<Patrimonio>, IRepositorioPatrimonio
    {
        private readonly SqlContext _context;
        public RepositorioPatrimonio(SqlContext Context)
            : base(Context)
        {
            _context = Context;
        }
    }
}
