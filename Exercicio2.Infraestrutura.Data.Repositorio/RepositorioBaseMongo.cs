﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;
using MongoDB.Driver;
using ServiceStack;
using System;
using System.Collections.Generic;

namespace Exercicio2.Infraestrutura.Data.Repositorio
{
    public abstract class RepositorioBaseMongo<TEntity> : IDisposable, IRepositorioBase<TEntity> where TEntity : class
    {
        protected readonly MongoContext _context;
        protected readonly IMongoCollection<TEntity> DbSet;

        public RepositorioBaseMongo(MongoContext context)
        {
            _context = context;
            DbSet = _context.GetCollection<TEntity>(typeof(TEntity).Name);
        }

        public virtual void Add(TEntity obj)
        {
            DbSet.InsertOne(obj);
        }

        public virtual TEntity GetById(int id)
        {
            return DbSet.Find(Builders<TEntity>.Filter.Eq("_id", id)).SingleOrDefault();
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return DbSet.Find(Builders<TEntity>.Filter.Empty).ToList();
        }

        public virtual void Update(TEntity obj)
        {
            DbSet.ReplaceOne(Builders<TEntity>.Filter.Eq("_id", obj.GetId()), obj);
        }

        public virtual void Remove(TEntity obj)
        {
            DbSet.DeleteOne(Builders<TEntity>.Filter.Eq("_id", obj.GetId()));
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public IReadOnlyList<TEntity> Find(ISpecification<TEntity> specification, int page = 0, int pageSize = 100)
        {
            return DbSet.Find(Builders<TEntity>.Filter.Where(specification.Predicate)).ToList();
        }
    }
}
