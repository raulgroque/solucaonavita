﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications;
using Exercicio2.Infraestrutura.CrossCutting.Helpers.Specifications.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercicio2.Infraestrutura.Data.Repositorio
{
    public abstract class RepositorioBaseSQL<TEntity> : IDisposable, IRepositorioBase<TEntity> where TEntity : class
    {
        private readonly SqlContext _context;

        public RepositorioBaseSQL(SqlContext context)
        {
            _context = context;
        }

        public virtual void Add(TEntity obj)
        {
            try
            {
                _context.Set<TEntity>().Add(obj);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public virtual TEntity GetById(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }

        public virtual void Update(TEntity obj)
        {
            try
            {
                _context.Entry(obj).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public virtual void Remove(TEntity obj)
        {
            try
            {
                _context.Set<TEntity>().Remove(obj);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public virtual IReadOnlyList<TEntity> Find(ISpecification<TEntity> specification, int page = 0, int pageSize = 100)
        {
            return _context.Set<TEntity>()
                .Where(specification.Predicate)
                .Skip(page * pageSize)
                .Take(pageSize)
                .ToList();
        }

        public virtual void Dispose()
        {
            _context.Dispose();
        }
    }
}
