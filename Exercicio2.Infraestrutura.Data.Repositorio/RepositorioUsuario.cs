﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Dominio.Modelos.Sql;
using System.Linq;

namespace Exercicio2.Infraestrutura.Data.Repositorio
{
    public class RepositorioUsuario : RepositorioBaseSQL<Usuario>, IRepositorioUsuario
    {
        private readonly SqlContext _context;
        public RepositorioUsuario(SqlContext Context)
            : base(Context)
        {
            _context = Context;
        }

        public Usuario GetByLoginPassword(string login, string password)
        {
            return _context.Usuario
                .Where(u => u.Email == login && u.Senha == password).FirstOrDefault();
        }
    }
}