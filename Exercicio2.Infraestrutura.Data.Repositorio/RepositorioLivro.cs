﻿using Exercicio2.Dominio.Core.Interfaces.Repositorios;
using Exercicio2.Dominio.Modelos.Mongo;
using MongoDB.Driver;

namespace Exercicio2.Infraestrutura.Data.Repositorio
{
    public class RepositorioLivro : RepositorioBaseMongo<Livro>, IRepositorioLivro
    {
        private readonly MongoContext _context;

        public RepositorioLivro(MongoContext Context)
            : base(Context)
        {
            _context = Context;
        }

        public Livro GetByAutor(string autor)
        {
            return DbSet.
                Find(Builders<Livro>.Filter.Where(l => l.Autor == autor)).SingleOrDefault();
        }
    }
}
